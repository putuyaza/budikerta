<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
      <p><?php echo $_SESSION['nama']; ?></p>
        <a href="#"><i class=""></i></a>
      </div>
    </div>
    <!-- search form -->

    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header"></li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-dashboard"></i> <span>Dashboard</span>
           <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
         <ul class="treeview-menu">
          <li><a href="../karyawan/dasboardkaryawan.php" >Home</a></li>

        </ul>
      </li>

       <li class="treeview">
        <a href="#">
          <i class="fa fa-list"></i> <span>Obat</span>
           <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
         <ul class="treeview-menu">

            <li><a href="../karyawan/cariobat.php" >Cari Obat</a></li>
        </ul>
      </li>



    </ul>
  </section>
  <!-- /.sidebar -->
</aside>

<!-- =============================================== -->
