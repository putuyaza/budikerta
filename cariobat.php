<?php

include('./template/headeradmin.php');

?>

<?php

include('./template/menuadmin.php');

?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    

    <!-- Main content -->
    <section class="content">

     
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Obat</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap"><div class="row"><div class="col-sm-6"></div><div class="col-sm-6"></div></div><div class="row"><div class="col-sm-12"><table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                <thead>
                <tr role="row"><th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Nama Obat: activate to sort column descending">Nama Obat</th><th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="aturan Pakai: activate to sort column ascending">Aturan Pakai</th><th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Jenis Obat: activate to sort column ascending">Jenis Obat</th><th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Distributor: activate to sort column ascending">Distributor</th><th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="indikasi: activate to sort column ascending">indikasi</th><th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Kandungan: activate to sort column ascending">Kandungan</th></tr>
                </thead>
                <tbody>
                <tr role="row" class="odd">
                  <td class="sorting_1">Paracetamol Tab</td>
                  <td>3 x 1</td>
                  <td>Tablet</td>
                  <td>Pt PIM</td>
                  <td>Meredakan Demam</td>
                  <td>paracetamol</td>
                </tr><tr role="row" class="even">
                  <td class="sorting_1">Mixalgin</td>
                  <td>2 x 1</td>
                  <td>Tablet</td>
                  <td>Pt Erela</td>
                  <td>Sakit kepala, Nyeri otot</td>
                  <td>Methampyron 500 mg, caffein 500</td>
                </tr><tr role="row" class="odd">
                  <td class="sorting_1">Piroxicam 2o ml</td>
                  <td>3 x 1</td>
                  <td>Kapsul</td>
                  <td>Pt Kimiafarma</td>
                  <td>Encok, Asam urat</td>
                  <td>Piroxicam</td>
                </tr><tr role="row" class="even">
                  <td class="sorting_1">Gecko</td>
                  <td>Firefox 3.0</td>
                  <td>Win 2k+ / OSX.3+</td>
                  <td>1.9</td>
                  <td>A</td>
                  <td>b</td>
                </tr></tbody>
                <tfoot>
                <tr><th rowspan="1" colspan="1">Nama Obat</th><th rowspan="1" colspan="1">Aturan Pakai</th><th rowspan="1" colspan="1">Jenis Obat</th><th rowspan="1" colspan="1">Distributor</th><th rowspan="1" colspan="1">Indikasi</th><th rowspan="1" colspan="1">Kandungan</th></tr>
                </tfoot>
              </table></div></div><div class="row"><div class="col-sm-5"><div class="dataTables_info" id="example2_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div></div><div class="col-sm-7"><div class="dataTables_paginate paging_simple_numbers" id="example2_paginate"><ul class="pagination"><li class="paginate_button previous disabled" id="example2_previous"><a href="#" aria-controls="example2" data-dt-idx="0" tabindex="0">Previous</a></li><li class="paginate_button active"><a href="#" aria-controls="example2" data-dt-idx="1" tabindex="0">1</a></li><li class="paginate_button "><a href="#" aria-controls="example2" data-dt-idx="2" tabindex="0">2</a></li><li class="paginate_button "><a href="#" aria-controls="example2" data-dt-idx="3" tabindex="0">3</a></li><li class="paginate_button "><a href="#" aria-controls="example2" data-dt-idx="4" tabindex="0">4</a></li><li class="paginate_button "><a href="#" aria-controls="example2" data-dt-idx="5" tabindex="0">5</a></li><li class="paginate_button "><a href="#" aria-controls="example2" data-dt-idx="6" tabindex="0">6</a></li><li class="paginate_button next" id="example2_next"><a href="#" aria-controls="example2" data-dt-idx="7" tabindex="0">Next</a></li></ul></div></div></div></div>
            </div>
            <!-- /.box-body -->
          </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php

include('./template/footer.php');

?>