-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 29 Sep 2019 pada 13.50
-- Versi server: 10.3.16-MariaDB
-- Versi PHP: 7.2.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sispeckbat`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbdistributor`
--

CREATE TABLE `tbdistributor` (
  `iddistributor` int(6) NOT NULL,
  `namaobat` varchar(50) NOT NULL,
  `alamat` int(100) NOT NULL,
  `kota` int(30) NOT NULL,
  `notlp` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbjenisobat`
--

CREATE TABLE `tbjenisobat` (
  `idjenisobat` int(6) NOT NULL,
  `jenisobat` varchar(30) NOT NULL,
  `komposisiobat` varchar(20) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  `indikasi` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbjenisobat`
--

INSERT INTO `tbjenisobat` (`idjenisobat`, `jenisobat`, `komposisiobat`, `keterangan`, `indikasi`) VALUES
(6, 'tablet 2', 'nitrogen', 'obat sakit tendas', 'sakit kepala'),
(7, 'generik', 'bubuk', 'obat bubuk', 'kejang'),
(8, 'syrup', 'paracet', 'obat keras', 'flu');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbobat`
--

CREATE TABLE `tbobat` (
  `idobat` int(6) NOT NULL,
  `namaobat` varchar(50) NOT NULL,
  `harga` double(8,0) NOT NULL,
  `distributor` varchar(30) NOT NULL,
  `jenisobat` varchar(50) NOT NULL,
  `aturanpakai` varchar(30) NOT NULL,
  `indikasi` text NOT NULL,
  `komposisi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbobat`
--

INSERT INTO `tbobat` (`idobat`, `namaobat`, `harga`, `distributor`, `jenisobat`, `aturanpakai`, `indikasi`, `komposisi`) VALUES
(1, 'kiranti', 20000, 'PT. Maju Mundur', 'tablet', '10x1 hari', 'nyeri', 'nitrogen\r\n'),
(5, 'hhaofaohy', 30000, 'bsuagu', 'syrup', 'hdsuiashg', 'kejang', 'bubuk');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbuser`
--

CREATE TABLE `tbuser` (
  `idpassword` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(10) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `notlp` varchar(15) NOT NULL,
  `level` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbuser`
--

INSERT INTO `tbuser` (`idpassword`, `username`, `password`, `nama`, `alamat`, `notlp`, `level`) VALUES
(1, 'superadmin', '12345678', 'superadmin', 'denpasar', '081234567888', 'admin'),
(2, 'manager', 'manager', 'budi', 'tabanan', '0909090', 'manager'),
(3, 'jery1', '12345678', 'jery', 'tabanan', '098999', 'karyawan');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tbdistributor`
--
ALTER TABLE `tbdistributor`
  ADD PRIMARY KEY (`iddistributor`);

--
-- Indeks untuk tabel `tbjenisobat`
--
ALTER TABLE `tbjenisobat`
  ADD PRIMARY KEY (`idjenisobat`);

--
-- Indeks untuk tabel `tbobat`
--
ALTER TABLE `tbobat`
  ADD PRIMARY KEY (`idobat`);

--
-- Indeks untuk tabel `tbuser`
--
ALTER TABLE `tbuser`
  ADD PRIMARY KEY (`idpassword`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tbdistributor`
--
ALTER TABLE `tbdistributor`
  MODIFY `iddistributor` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbjenisobat`
--
ALTER TABLE `tbjenisobat`
  MODIFY `idjenisobat` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `tbobat`
--
ALTER TABLE `tbobat`
  MODIFY `idobat` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `tbuser`
--
ALTER TABLE `tbuser`
  MODIFY `idpassword` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
