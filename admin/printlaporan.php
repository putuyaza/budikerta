<?php
include('../koneksi.php');
session_start();
if(!isset($_SESSION['admin'])){

  header('location:../index.php?msglog=msglog');
}


 ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Print Laporan</title>
    <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
      <link rel="stylesheet" href="../css/jquery.dataTables.css">
      <style media="print">
        .btprint{
          display: none;
        }
        .tblp{
          width:100%;padding:2%;margin-top:5%;margin-bottom:3%;margin-right:2%;font-size:2rem;
        }
      </style>

      <style media="screen">
      .tblp{
        width:100%;padding:2%;margin-top:5%;margin-bottom:3%;margin-right:2%;font-size:2rem;
      }
      </style>
  </head>
  <body>
    <div class="container">
      <h1 style="text-align:center;">LAPORAN DATA OBAT</h1>
      <h1 style="text-align:center;">PT BALI CAKRA KUSUMA </h1>
      <table border="1" cellspacing="1" class="tblp">
    <thead>
        <tr >
            <th>No</th>
            <th>Nama Obat</th>
            <th>Harga</th>
            <th>Jenis Obat</th>
            <th>Komposisi</th>
            <th>Indikasi</th>
            <th>Aturan pakai</th>
            <th>Distributor</th>
        </tr>
    </thead>
    <tbody>
      <?php
      include('../koneksi.php');
      $no=1;
      $sql ="SELECT * FROM tbobat";
      $data = mysqli_query($konek,$sql);
      while ($hasil=mysqli_fetch_array($data)) {
      ?>
          <tr>
              <td><?php echo $no++; ?></td>
              <td><?php echo $hasil['namaobat']; ?></td>
              <td><?php echo $hasil['harga']; ?></td>
              <td><?php echo $hasil['jenisobat'];?></td>
              <td><?php echo $hasil['komposisi'];?></td>
              <td><?php echo $hasil['indikasi']; ?></td>
              <td><?php echo $hasil['aturanpakai']; ?></td>
              <td><?php echo $hasil['distributor']; ?></td>
          </tr>
            <?php } ?>
    </tbody>

    </table>
    <table style="float:right;">
      <tr>
        <td >Mengetahui,...............</td>
      </tr>
      <tr>
        <td>Manager</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td></td>

      </tr>
      <tr>
        <td><br></br><br></br></td>
      </tr>
      <tr>
        <td>Drs Made Wartanan</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td></td>
      </tr>
    </table>
    </div>

    <button   onclick="window.print();" class="btn btn-block btn-primary btn-lg btprint">Print</button>



  </body>
  <script src="../bower_components/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- SlimScroll -->

  <script src="../js/jquery.dataTables.js"></script>

</html>
