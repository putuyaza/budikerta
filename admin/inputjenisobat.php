<?php
include('../template/headeradmin.php');

?>

<?php
include('../template/menuadmin.php');

?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Input Jenis Obat</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">

               <form role="form" action="prosesjenisobat.php" method="post">
               <div class="row">

               <!-- form control mulai -->
               <div class="col-md-6">
                <div class="form-group">
                  <label>jenis Obat</label>
                  <input type="text" class="form-control" name="jenisobat" id="exampleInputEmail1" placeholder="jenis obat">
                </div>
                </div>
               <div class="col-md-6">
                <div class="form-group">
                  <label>Komposisi Obat</label>
                  <input type="text" class="form-control" name="komposisi" id="exampleInputEmail1" placeholder="Komposisi Obat">
                </div>
                </div>
                 <!-- form control selesai -->
                 <!-- form control mulai -->
               <div class="col-md-6">
                <div class="form-group">
                  <label>Keterangan</label>
                  <input type="text" class="form-control" name="keterangan" id="exampleInputEmail1" placeholder="Keterangan">
                </div>
                </div>
                 <!-- form control selesai -->

                 <div class="col-md-6">
                  <div class="form-group">
                    <label>Indikasi</label>
                    <input type="text" class="form-control" name="indikasi" id="exampleInputEmail1" placeholder="Indikasi">
                  </div>
                  </div>
              </div>
              <div class="box-footer">
                <button type="simpan" class="btn btn-primary">simpan</button>

              </div>

            </form>
            </div>

          <!-- /.box -->
        </div>
        <!-- Default box -->
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">View Jenis Obat</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                      title="Collapse">
                <i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
            <table id="table_id" class="display">
        <thead>
            <tr>
                <th>No</th>
                <th>Jenis Obat</th>
                <th>Komposisi</th>
                <th>Keterangan</th>
                <th>Indikasi</th>
                <th>Opsi</th>
            </tr>
        </thead>
        <tbody>
          <?php
          include('../koneksi.php');
          $no=1;
          $sql ="SELECT * FROM tbjenisobat";
          $data = mysqli_query($konek,$sql);
          while ($hasil=mysqli_fetch_array($data)) {
          ?>
              <tr>
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $hasil['jenisobat']; ?></td>
                  <td><?php echo $hasil['komposisiobat'];?></td>
                  <td><?php echo $hasil['keterangan'];?></td>
                  <td><?php echo $hasil['indikasi']; ?></td>
                  <td><a link href="#" data-toggle="modal" data-target="#modal-default<?php echo $hasil['idjenisobat']?>"><i class="fa fa-edit"></i></a> | <a href="hapusjenisobat.php?id=<?= $hasil['idjenisobat'];  ?>" onclick="javascript:return confirm('anda yakin menghapus jenis obat ini..?')"><i class="fa fa-trash"></i></a></td>
              </tr>
                <?php } ?>
        </tbody>
    </table>


            <!-- /.box -->
          </div>
        <!-- /.box-body -->

        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- modal edit start -->
  <?php
  include('../koneksi.php');
  $no=1;
  $sql ="SELECT * FROM tbjenisobat";
  $data = mysqli_query($konek,$sql);
  while ($hasil=mysqli_fetch_array($data)) {
  ?>
  <div class="modal fade" id="modal-default<?php echo $hasil['idjenisobat'];?>">
         <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                 <span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title">Edit jenis obat</h4>
             </div>
              <form name="edituser" action="proseseditjenisobat.php" method="post">
             <div class="modal-body">

                <input type="hidden" class="form-control" name="idjenisobat" id="exampleInputEmail1" value="<?php echo $hasil['idjenisobat'] ?>">
                <label>Jenis Obat</label>
                <input type="text" class="form-control" name="jenisobat" id="exampleInputEmail1" value="<?php echo $hasil['jenisobat'] ?>" placeholder="Enter jenisobat">
                 <label>Komposisi</label>
               <input type="text" class="form-control" name="komposisi" id="exampleInputEmail1" value="<?php echo $hasil['komposisiobat'] ?>" placeholder="Enter komposisi obat">
                <label>Keterangan</label>
              <input type="text" class="form-control" name="keterangan" id="exampleInputEmail1" value="<?php echo $hasil['keterangan'] ?>" placeholder="Enter keterangan obat">
               <label>Indikasi</label>
               <input type="text" class="form-control" name="indikasi" id="exampleInputEmail1" value="<?php echo $hasil['indikasi'] ?>" placeholder="Enter Indikasi">

             </div>
             <div class="modal-footer">
               <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
               <button type="submit" class="btn btn-primary">Save changes</button>
             </div>
               </form>
           </div>
           <!-- /.modal-content -->
         </div>
         <!-- /.modal-dialog -->
       </div>
       <!-- /.modal -->
    <?php } ?>
  <!-- end modal edit -->


  <?php
include('../template/footer.php');

?>
