<?php
include('../template/headeradmin.php');

?>

<?php
include('../template/menuadmin.php');

?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Input User</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">

               <form role="form" action="prosestambahuser.php" method="post">
               <div class="row">

               <!-- form control mulai -->
               <div class="col-md-6">
                <div class="form-group">
                  <label>Nama</label>
                  <input type="text" class="form-control" name="nama" id="exampleInputEmail1" placeholder="Nama">
                </div>
                </div>
                <div class="col-md-6">
                 <div class="form-group">
                   <label>Email</label>
                   <input type="text" class="form-control" name="email" id="exampleInputEmail1" placeholder="Email">
                 </div>
                 </div>
                 <!-- form control selesai -->
                 <!-- form control mulai -->
               <div class="col-md-6">
                <div class="form-group">
                  <label>Alamat</label>
                  <input type="text" class="form-control" name="alamat" id="exampleInputEmail1" placeholder="Alamat">
                </div>
                </div>
                 <!-- form control selesai -->
               <!-- form control mulai -->
               <div class="col-md-6">
                <div class="form-group">
                  <label>Username</label>
                  <input type="text" class="form-control" name="username" id="exampleInputEmail1" placeholder="Username">
                </div>
                </div>
                 <!-- form control selesai -->
              <!-- form control mulai -->
               <div class="col-md-6">
                <div class="form-group">
                  <label >No Tlp</label>
                  <input type="text" class="form-control"name="no_tlp" id="exampleInputEmail1" placeholder="no_tlp">
                </div>
                </div>
                 <!-- form control selesai -->

                <div class="col-md-6">
                <div class="form-group">
                  <label >Password</label>
                  <input type="password" class="form-control" name="password"id="exampleInputPassword1" placeholder="Password">
                </div>
               </div>


                <!-- form control mulai -->
               <div class="col-md-6">
                <div class="form-group">
                  <label >Level</label>
                  <select class="form-control" name="level">
                    <option value="admin">Admin</option>
                    <option value="karyawan">Karyawan</option>
                    <option value="manager">Manager</option>


                  </select>
                </div>
                </div>
                 <!-- form control selesai -->

               </div>
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>&nbsp;<button type="reset" class="btn btn-danger">Reset</button>

              </div>

            </form>
            </div>

          <!-- /.box -->








        </div>
        <!-- /.box-body -->

      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

 <?php
include('../template/footer.php');

?>
