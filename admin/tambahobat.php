<?php
include('../template/headeradmin.php');

?>

<?php
include('../template/menuadmin.php');

?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Input Obat</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">

               <form role="form" action="prosestambahobat.php" method="post">
               <div class="row">

               <!-- form control mulai -->
               <div class="col-md-6">
                <div class="form-group">
                  <label>Nama Obat</label>
                  <input type="text" class="form-control" name="namaobat" id="exampleInputEmail1" placeholder="Nama Obat">
                </div>
                </div>
                <div class="col-md-6">
                 <div class="form-group">
                   <label>Harga</label>
                   <input type="text" class="form-control" name="harga" id="exampleInputEmail1" placeholder="Harga">
                 </div>
                 </div>
                 <!-- form control selesai -->
                 <!-- form control mulai -->
               <div class="col-md-6">
                <div class="form-group">
                  <label>Distributor</label>
                  <input type="text" class="form-control" name="distributor" id="exampleInputEmail1" placeholder="Distributor">
                </div>
                </div>
                 <!-- form control selesai -->
               <!-- form control mulai -->
               <div class="col-md-6">
                <div class="form-group">
                  <label>Jenis Obat</label>
                  <select class="form-control" name="jenisobat">
                    <?php
                    include('../koneksi.php');
                    $no=1;
                    $sql ="SELECT * FROM tbjenisobat";
                    $data = mysqli_query($konek,$sql);
                    while ($hasil=mysqli_fetch_array($data)) {
                    ?>
                    <option value="<?php echo $hasil['jenisobat'] ?>"><?php echo $hasil['jenisobat'] ?></option>
                  <?php } ?>
                  </select>
                </div>
                </div>
                 <!-- form control selesai -->
              <!-- form control mulai -->
              <div class="col-md-6">
               <div class="form-group">
                 <label>Indikasi</label>
                 <select class="form-control" name="indikasi">
                   <?php
                   include('../koneksi.php');
                   $no=1;
                   $sql ="SELECT * FROM tbjenisobat";
                   $data = mysqli_query($konek,$sql);
                   while ($hasil=mysqli_fetch_array($data)) {
                   ?>
                   <option value="<?php echo $hasil['indikasi'] ?>"><?php echo $hasil['indikasi'] ?></option>
                 <?php } ?>
                 </select>
               </div>
               </div>
                 <!-- form control selesai -->
                  <!-- form control mulai -->
                  <div class="col-md-6">
                   <div class="form-group">
                     <label>Komposisi</label>
                     <select class="form-control" name="komposisi">
                       <?php
                       include('../koneksi.php');
                       $no=1;
                       $sql ="SELECT * FROM tbjenisobat";
                       $data = mysqli_query($konek,$sql);
                       while ($hasil=mysqli_fetch_array($data)) {
                       ?>
                       <option value="<?php echo $hasil['komposisiobat'] ?>"><?php echo $hasil['komposisiobat'] ?></option>
                     <?php } ?>
                     </select>
                   </div>
                   </div>
                 <!-- form control selesai -->

                <div class="col-md-6">
                <div class="form-group">
                  <label >Aturan Pakai</label>
                  <input type="text" class="form-control" name="aturanpakai" id="exampleInputPassword1" placeholder="Aturan Pakai">
                </div>
               </div>
               </div>
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>&nbsp;<button type="reset" class="btn btn-danger">Reset</button>

              </div>

            </form>
            </div>

          <!-- /.box -->


          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">View Obat</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                  <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                  <i class="fa fa-times"></i></button>
              </div>
            </div>
          <div class="box-body">

            <table id="table_id" class="display">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Obat</th>
                <th>Harga</th>
                <th>Jenis Obat</th>
                <th>Komposisi</th>
                <th>Indikasi</th>
                <th>Aturan pakai</th>
                <th>Distributor</th>
                <th>Opsi</th>
            </tr>
        </thead>
        <tbody>
          <?php
          include('../koneksi.php');
          $no=1;
          $sql ="SELECT * FROM tbobat";
          $data = mysqli_query($konek,$sql);
          while ($hasil=mysqli_fetch_array($data)) {
          ?>
              <tr>
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $hasil['namaobat']; ?></td>
                  <td><?php echo $hasil['harga']; ?></td>
                  <td><?php echo $hasil['jenisobat'];?></td>
                  <td><?php echo $hasil['komposisi'];?></td>
                  <td><?php echo $hasil['indikasi']; ?></td>
                  <td><?php echo $hasil['aturanpakai']; ?></td>
                  <td><?php echo $hasil['distributor']; ?></td>
                  <td><a link href="#" data-toggle="modal" data-target="#modal-default<?php echo $hasil['idobat']?>"><i class="fa fa-edit"></i></a> | <a href="hapusobat.php?id=<?= $hasil['idobat'];  ?>" onclick="javascript:return confirm('anda yakin menghapus data obat ini..?')"><i class="fa fa-trash"></i></a></td>
              </tr>
                <?php } ?>
        </tbody>
    </table>
              </div>

        </div>
        <!-- /.box-body -->

        <!-- /.box-footer-->
      </div>
    </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- modal edit start -->
  <?php
  include('../koneksi.php');
  $no=1;
  $sql ="SELECT * FROM tbobat";
  $data = mysqli_query($konek,$sql);
  while ($hasil=mysqli_fetch_array($data)) {
  ?>
  <div class="modal fade" id="modal-default<?php echo $hasil['idobat']?>">
         <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                 <span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title">Edit obat</h4>
             </div>
             <form action="updateobat.php" method="POST">
             <div class="modal-body">
               <label>Nama Obat</label>
                <input type="hidden" class="form-control" name="idobat" id="exampleInputEmail1" value="<?= $hasil['idobat'] ?>" placeholder="Nama Obat">
               <input type="text" class="form-control" name="namaobat" id="exampleInputEmail1" value="<?= $hasil['namaobat'] ?>" placeholder="Nama Obat">

               <label>Harga</label>
                <input type="text" class="form-control" name="harga" id="exampleInputEmail1" value="<?= $hasil['harga'] ?>" placeholder="harga">

               <label>Jenis Obat</label>
               <select class="form-control" name="jenisobat">
                 <?php
                 $sql="SELECT * FROM tbjenisobat";
                 $data1= mysqli_query($konek,$sql);
                 while ($hasil1=mysqli_fetch_array($data1)) {
                  ?>
                 <option value="<?php echo $hasil['jenisobat'] ?>" selected><?php echo $hasil['jenisobat'] ?></option>
                 <option value="<?php echo $hasil1['jenisobat'] ?>"><?php echo $hasil1['jenisobat'] ?></option>
               <?php } ?>
               </select>
               <label>Komposisi</label>
               <select class="form-control" name="komposisi">
                 <?php
                 $sql="SELECT * FROM tbjenisobat";
                 $data1= mysqli_query($konek,$sql);
                 while ($hasil1=mysqli_fetch_array($data1)) {
                  ?>
                  <option value="<?php echo $hasil['komposisi'] ?>"><?php echo $hasil['komposisi'] ?></option>
                 <option value="<?php echo $hasil1['komposisiobat'] ?>"><?php echo $hasil1['komposisiobat'] ?></option>
               <?php } ?>
               </select>
               <label>Indikasi</label>
               <select class="form-control" name="indikasi">
                 <?php
                 $sql="SELECT * FROM tbjenisobat";
                 $data1= mysqli_query($konek,$sql);
                 while ($hasil1=mysqli_fetch_array($data1)) {
                  ?>
                  <option value="<?php echo $hasil['indikasi'] ?>"><?php echo $hasil['indikasi'] ?></option>
                 <option value="<?php echo $hasil1['indikasi'] ?>"><?php echo $hasil1['indikasi'] ?></option>
               <?php } ?>
               </select>

               <label>Distributor</label>
               <input type="text" class="form-control" name="distributor" id="exampleInputEmail1" value="<?= $hasil['distributor'] ?>" placeholder="Distributor">
               <label>Aturan Pakai</label>
               <input type="text" class="form-control" name="aturanpakai" id="exampleInputEmail1" value="<?= $hasil['aturanpakai'] ?>" placeholder="Aturan Pakai">
            </div>
             <div class="modal-footer">
               <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
               <button type="submit" class="btn btn-primary">Save changes</button>
             </div>
           </form>
           </div>
           </div>
           <!-- /.modal-content -->
         </div>
         <!-- /.modal-dialog -->
       </div>
       <!-- /.modal -->
    <?php } ?>
  <!-- end modal edit -->
 <?php
include('../template/footer.php');

?>
