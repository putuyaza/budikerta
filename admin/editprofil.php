<?php

include('../template/headeradmin.php');

?>

<?php

include('../template/menuadmin.php');

?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Edit Profil</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
               <form role="form" action="updateprofil.php" method="post">
                 <?php
                 include('../koneksi.php');
                 $id=$_GET['id'];
                 $no=1;
                 $sql ="SELECT * FROM tbuser WHERE idpassword='$id'";
                 $data = mysqli_query($konek,$sql);
                 while ($hasil=mysqli_fetch_array($data)) {
                 ?>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                      <label for="exampleInputEmail1">Nama</label>
                      <input type="text" name="nama" value="<?php echo $hasil['nama']; ?>" class="form-control" id="exampleInputEmail1" placeholder="Enter Nama">
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                      <label for="exampleInputEmail1">username</label>
                      <input type="hidden" name="id" value="<?php echo $hasil['idpassword']; ?>">
                      <input type="text" value="<?php echo $hasil['username']; ?>" name="username" class="form-control" id="exampleInputEmail1" placeholder="username">
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                      <label for="exampleInputEmail1">Password</label>
                      <input type="text" value="<?php echo $hasil['password']; ?>" name="password" class="form-control" id="exampleInputEmail1" placeholder="password">
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                      <label for="exampleInputEmail1">Alamat</label>
                      <input type="text" value="<?php echo $hasil['alamat']; ?>" name="alamat" class="form-control" id="exampleInputEmail1" placeholder="Alamat">
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                      <label for="exampleInputEmail1">No Telpon</label>
                      <input type="text" value="<?php echo $hasil['notlp']; ?>" name="notlp" class="form-control" id="exampleInputEmail1" placeholder="No Telpon">
                  </div>
                </div>
                <div class="col-md-6">
                 <div class="form-group">
                   <label >Level</label>
                   <select class="form-control" name="level">
                     <option value="<?php echo $hasil['level']?>"><?php echo $hasil['level']?></option>
                     <option value="admin">Admin</option>
                     <option value="karyawan">Karyawan</option>
                     <option value="manager">Manager</option>


                   </select>
                 </div>

                               </div>


            </div>
          <?php } ?>

            <!-- /.box-body -->

          </div>
          <div class="box-footer">
            <button type="submit" class="btn btn-primary">Update</button>
  </form>
          </div>
    </section>
    <!-- /.content -->
  </div>

  <!-- /.content-wrapper -->

  <?php

include('../template/footer.php');

?>


<!--jAVA SCRIPT datatables-->
