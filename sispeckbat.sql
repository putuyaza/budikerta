-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 24, 2019 at 04:46 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sispeckbat`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbdistributor`
--

CREATE TABLE `tbdistributor` (
  `iddistributor` int(6) NOT NULL,
  `namaobat` varchar(50) NOT NULL,
  `alamat` int(100) NOT NULL,
  `kota` int(30) NOT NULL,
  `notlp` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbjenisobat`
--

CREATE TABLE `tbjenisobat` (
  `idjenisobat` int(6) NOT NULL,
  `modelobat` varchar(20) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  `indikasi` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbobat`
--

CREATE TABLE `tbobat` (
  `idobat` int(6) NOT NULL,
  `namaobat` varchar(50) NOT NULL,
  `distributor` varchar(30) NOT NULL,
  `komposisi` varchar(100) NOT NULL,
  `indikasi` varchar(70) NOT NULL,
  `aturanpakai` varchar(30) NOT NULL,
  `jenisobat` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbobat`
--

INSERT INTO `tbobat` (`idobat`, `namaobat`, `distributor`, `komposisi`, `indikasi`, `aturanpakai`, `jenisobat`) VALUES
(1, 'chfjy', '', '', '', 'fjfjgjk', ''),
(2, 'dhujgj', '', '', '', 'fjfkgk', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbuser`
--

CREATE TABLE `tbuser` (
  `idpassword` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(10) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `notlp` varchar(15) NOT NULL,
  `level` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbuser`
--

INSERT INTO `tbuser` (`idpassword`, `username`, `password`, `nama`, `alamat`, `notlp`, `level`) VALUES
(4, 'budi1', '123', 'budi aja', 'gfffg', '767676', 'manager'),
(5, 'budi', '123', 'bfhff', 'ngvhg', '6776', 'admin'),
(6, 'putu', '123', 'hfhfhf', 'ffhf', 'gfhfgf', 'karyawan'),
(7, '', '123', '', '', '083117767037', 'level'),
(8, 'hdhfjshjh', '123', 'budi kerta', 'tabanan', '083117767037', 'level'),
(9, 'budik', '123', 'I Wayan Budi Kerta Jaya', 'TABANAN', '083117767037', 'level'),
(10, 'budik', '123', 'Budi keta jaya ', 'TABANAN', '083117767037', 'karyawan'),
(11, 'budi', '123', 'budi kerta', 'TABANAN', '083117767037', 'manager'),
(12, 'budik', '123', 'budi kerta', '', '083117767037', 'admin'),
(13, 'budik', '123', 'budi kerta', 'TABANAN', '083117767037', 'karyawan'),
(14, '', '123', 'budi', '', '083117767037', 'admin'),
(15, 'budikj', '123', 'budij', 'TABANAN', '083117767037', 'karyawan'),
(16, 'ytyty', 'yfgilh', 'y8yihoho', '083117767037', '123', 'level'),
(17, 'bud', '', '', '083117767037', '123', 'level'),
(18, 'I Wayan Budi Kerta J', 'TABANAN', '123', '083117767037', '123', 'level');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbdistributor`
--
ALTER TABLE `tbdistributor`
  ADD PRIMARY KEY (`iddistributor`);

--
-- Indexes for table `tbjenisobat`
--
ALTER TABLE `tbjenisobat`
  ADD PRIMARY KEY (`idjenisobat`);

--
-- Indexes for table `tbobat`
--
ALTER TABLE `tbobat`
  ADD PRIMARY KEY (`idobat`);

--
-- Indexes for table `tbuser`
--
ALTER TABLE `tbuser`
  ADD PRIMARY KEY (`idpassword`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbdistributor`
--
ALTER TABLE `tbdistributor`
  MODIFY `iddistributor` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbjenisobat`
--
ALTER TABLE `tbjenisobat`
  MODIFY `idjenisobat` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbobat`
--
ALTER TABLE `tbobat`
  MODIFY `idobat` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbuser`
--
ALTER TABLE `tbuser`
  MODIFY `idpassword` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
