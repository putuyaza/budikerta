
<?php

include('../template/headermanager.php');

?>

<?php

include('../template/menumanager.php');

?>
    <link rel="stylesheet" href="../assets/css/jquery.dataTables.min.css"/>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">


          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Laporan Obat</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="table_id" class="display">
          <thead>
              <tr>
                  <th>No</th>
                  <th>Nama Obat</th>
                  <th>Harga</th>
                  <th>Jenis Obat</th>
                  <th>Komposisi</th>
                  <th>Indikasi</th>
                  <th>Aturan pakai</th>
                  <th>Distributor</th>
              </tr>
          </thead>
          <tbody>
            <?php
            include('../koneksi.php');
            $no=1;
            $sql ="SELECT * FROM tbobat";
            $data = mysqli_query($konek,$sql);
            while ($hasil=mysqli_fetch_array($data)) {
            ?>
                <tr>
                    <td><?php echo $no++; ?></td>
                    <td><?php echo $hasil['namaobat']; ?></td>
                    <td><?php echo $hasil['harga']; ?></td>
                    <td><?php echo $hasil['jenisobat'];?></td>
                    <td><?php echo $hasil['komposisi'];?></td>
                    <td><?php echo $hasil['indikasi']; ?></td>
                    <td><?php echo $hasil['aturanpakai']; ?></td>
                    <td><?php echo $hasil['distributor']; ?></td>
                </tr>
                  <?php } ?>
          </tbody>

      </table>
          <a href="printlaporan.php" class="btn btn-block btn-primary btn-lg">Print</a>
            </div>
            <!-- /.box-body -->

          </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php

include('../template/footer.php');

?>

   <script src="../assets/js/jquery.dataTables.min.js"></script>

<script>


     $(document).ready(function(){
            $('#table_id').datatable();
            });
</script>
<!--jAVA SCRIPT datatables-->
