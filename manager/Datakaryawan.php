<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Datakaryawan extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  public function getAll(){
    $this->db->select('*');
    $this->db->from('user');
    $this->db->join('jabatan','jabatan.IdJabatan=user.IdJabatan');
    return $this->db->get();
  }

  public function countkaryawan(){
    $sql=" SELECT count(Level) AS Level FROM user WHERE Level='karyawan'";
    $hasil = $this->db->query($sql);
    return $hasil->row()->Level;
  }
  public function countadmin(){
    $sql=" SELECT count(Level) AS Level FROM user WHERE Level='administrator'";
    $hasil = $this->db->query($sql);
    return $hasil->row()->Level;
  }
  public function countmanager(){
    $sql=" SELECT count(Level) AS Level FROM user WHERE Level='manager'";
    $hasil = $this->db->query($sql);
    return $hasil->row()->Level;
  }
  public function countbod1(){
    $sql=" SELECT count(Level) AS Level FROM user WHERE Level='CEO'";
    $hasil = $this->db->query($sql);
    return $hasil->row()->Level;
  }


}
