<?php

include('../template/headermanager.php');

?>

<?php

include('../template/menumanager.php');

?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Lihat User</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <table id="table_id" class="display">
      <thead>
          <tr>
              <th>No</th>
              <th>Username</th>
              <th>Nama</th>
              <th>Alamat</th>
              <th>No Telpon</th>
              <th>Level</th>
              <th>Opsi</th>
          </tr>
      </thead>
      <tbody>
        <?php
        include('../koneksi.php');
        $no=1;
        $sql ="SELECT * FROM tbuser";
        $data = mysqli_query($konek,$sql);
        while ($hasil=mysqli_fetch_array($data)) {
        ?>
            <tr>
                <td><?php echo $no++; ?></td>
                <td><?php echo $hasil['username'];?></td>
                <td><?php echo $hasil['nama'];?></td>
                <td><?php echo $hasil['alamat']; ?></td>
                <td><?php echo $hasil['notlp'];?></td>
                <td><?php echo $hasil['level']; ?></td>
                <td><a link href="#" data-toggle="modal" data-target="#modal-default<?php echo $hasil['idpassword']?>"><i class="fa fa-edit"></i></a> | <a href="hapususer.php?id=<?= $hasil['idpassword'];  ?>" onclick="javascript:return confirm('anda yakin menghapus data ini.?')"><i class="fa fa-trash"></i></a></td>
            </tr>
              <?php } ?>
      </tbody>
  </table>
            </div>

          <!-- /.box -->

<!-- modal edit start -->
<?php
include('../koneksi.php');
$no=1;
$sql ="SELECT * FROM tbuser";
$data = mysqli_query($konek,$sql);
while ($hasil=mysqli_fetch_array($data)) {
?>
<div class="modal fade" id="modal-default<?php echo $hasil['idpassword'];?>">
       <div class="modal-dialog">
         <div class="modal-content">
           <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span></button>
             <h4 class="modal-title">Edit User</h4>
           </div>
            <form name="edituser" action="editdatauser.php" method="post">
           <div class="modal-body">
              <input type="hidden" class="form-control" name="id" id="exampleInputEmail1" value="<?php echo $hasil['idpassword'] ?>">
             <input type="text" class="form-control" name="username" id="exampleInputEmail1" value="<?php echo $hasil['username'] ?>" placeholder="Enter username">
             <input type="password" class="form-control" name="password" id="exampleInputEmail1" value="<?php echo $hasil['password'] ?>">
             <input type="text" class="form-control" name="nama" id="exampleInputEmail1" value="<?php echo $hasil['nama'] ?>" placeholder="Enter nama">
             <input type="text" class="form-control" name="alamat" id="exampleInputEmail1" value="<?php echo $hasil['alamat'] ?>" placeholder="Enter alamat">
             <input type="text" class="form-control" name="notlp" id="exampleInputEmail1" value="<?php echo $hasil['notlp'] ?>" placeholder="Enter no telpon">
             <select class="form-control" name="level">
                    <option value="<?php echo $hasil['level'] ?>"><?php echo $hasil['level'] ?></option>
                    <option value="admin">admin</option>
                    <option value="karyawan">Karyawan</option>
                    <option value="manager">Manager</option>
              </select>
           </div>
           <div class="modal-footer">
             <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
             <button type="submit" class="btn btn-primary">Save changes</button>
           </div>
             </form>
         </div>
         <!-- /.modal-content -->
       </div>
       <!-- /.modal-dialog -->
     </div>
     <!-- /.modal -->
  <?php } ?>
<!-- end modal edit -->






        </div>
        <!-- /.box-body -->

      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

 <?php
include('../template/footer.php');

?>
